package dao;


import entity.Task;
import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

import java.sql.SQLException;

@Local
@Stateless
public class TaskDao {
    private EntityTransaction transaction;
    private EntityManager entityManager;

    protected EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = Persistence.createEntityManagerFactory("postgresql").createEntityManager();
        }
        return entityManager;
    }

    private EntityTransaction getTransaction() {
        this.transaction = getEntityManager().getTransaction();
        return transaction;
    }

    public Task createTask(Task entity) throws SQLException {
        if (!this.getTransaction().isActive()) {
            this.getTransaction().begin();
        }
        try {
            getEntityManager().persist(entity);
            getEntityManager().flush();
            getTransaction().commit();
            return entity;
        }catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            if (this.getTransaction().isActive()) {
                this.getTransaction().begin();
            }
            getTransaction().rollback();
            throw new SQLException(e.getMessage());
        }
    }
}

