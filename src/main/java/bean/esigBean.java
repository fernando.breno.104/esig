package bean;

import dao.TaskDao;
import entity.Task;
import jakarta.ejb.EJB;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;


import java.io.Serializable;
import java.sql.SQLException;

@Named
@ViewScoped
public class esigBean implements Serializable {

    private Task task;
    @EJB
    TaskDao taskDao;

    public Task getTask() {
        if (this.task == null) {
            this.task = new Task();
        }
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public void createTask() throws SQLException {
        this.taskDao.createTask(this.task);
    }
}
